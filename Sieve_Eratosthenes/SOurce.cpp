#include<iostream>
#include<math.h>
using namespace std;
int main()
{

/*
Input: an integer n > 1
Let A be an array of Boolean values, indexed by integers 2 to n,
initially all set to true. 
for i = 2, 3, 4, ..., not exceeding vn:
 if A[i] is true:
   for j = i2, i2+i, i2+2i, ..., not exceeding n?:
     A[j] := false
*/
int n=100; //arr size. We could also dynamically allocate the array if we want to.
int i,j;
bool table[20];
for (i=2;i<=n;i++)
table[i]=true;
for (i=2;i<int(floor(sqrt(n)));i++)
{
  if(table[i]==true)
  {
    for(j=pow(i,2);j<n;j=j+i)
    {
    table[j] = false;
    }
  }
}

for(i=2;i<n;i++)
{
  if (table[i]==true)
  cout<<"\n prime ="<<i;
}
cout<<endl;
return 0;
}


