namespace A 
{
    namespace B 
    {
        class C{public: C(){};};
    }
}

class D 
{
    using C = A::B::C;
    //using A::B::C; Doesnt work !
    void func(C c);
};


void D::func(C c)
{
    int a = 3;
    a++;
    return;
}

#include <iostream>

int main()
{
    std::cout<<"Hello World";

    return 0;
}