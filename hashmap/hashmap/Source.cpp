#include <iostream>
#include <map>
#include <string>
#include <stdlib.h>
//This code will print frequency of all characters in a string
using namespace std;
void countfrequencies(string str);
int main()
{
	
	string str;
	cout<<"Enter a String >> ";
	cin>>str;
	countfrequencies(str);
	return 0;
}

void countfrequencies(string str)
{
	map <char,int> ftable;
	int i;
	i=str.length();
	
	for (int j=0;j<i;j++)
	ftable[str.at(j)]+=1;	//insert and increment values present in the hash-map. One line code :)

	for(map<char,int>::iterator it=ftable.begin();it!=ftable.end();it++)
	{
		cout<<(*it).first<<" == "<<(*it).second<<endl;
	}
	
}