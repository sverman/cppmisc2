#include <iostream>       
#include <thread>         
#include <mutex>          
#include <condition_variable>
std::mutex mtx;           
std::condition_variable cond[4];
bool ready[4] = {false,false,false,false};
int a =2,b=3,c=4,d=5,e=1;
using namespace std;
void print_block1 (int n, char c) {
	
	std::unique_lock<std::mutex> lck (mtx);
	cout <<"print_block1 :can wait here for a while\n";

	while(!ready[1])
		cond[1].wait(lck);
	e=e+e*b;

	for (int i=0; i<n; ++i) { std::cout << c; }
	std::cout << '\n';
	ready[3] = true;
	cond[3].notify_one();
	lck.unlock();
}

void print_block2 (int n, char c) {

	
	
	std::unique_lock<std::mutex> lck (mtx);
	cout <<"print_block2 :can wait here for a while\n";

	while(!ready[2])
		cond[2].wait(lck);
	
	e=e+e*c;
	for (int i=0; i<n; ++i) { std::cout << c; }
	std::cout << '\n';
	ready[1] =true;
	cond[1].notify_all();
	lck.unlock();
}

void print_block3 (int n, char c) {
	
	
	std::unique_lock<std::mutex> lck (mtx);
	cout <<"print_block3 :can wait here for a while\n";

	while(!ready[3])
		cond[3].wait(lck);

	e=e+e*d;
	for (int i=0; i<n; ++i) { std::cout << c; }
	std::cout << '\n';
	lck.unlock();
}
void print_block4 (int n, char c) {
		
	std::unique_lock<std::mutex> lck (mtx);
	cout <<"print_block4 :can wait here for a while\n";

	while(!ready[0])
	cond[0].wait(lck);
	e=e+e*a;

	for (int i=0; i<n; ++i) { std::cout << c; }
	std::cout << '\n';
	ready[2]=true;
	cond[2].notify_all();
	lck.unlock();
}
int main ()
{
	
	std::thread th1 (print_block1,50,'1');
	std::thread th2 (print_block2,50,'2');
	std::thread th3 (print_block3,50,'3');
	std::thread th4 (print_block4,50,'4');
	ready[0] = true;
	cond[0].notify_all();

	
	th1.join();
	
	th2.join();
	
	th3.join();
	th4.join();
	
	cout <<"This is one way of thread sequencing. \nAll the threads get launched in random order.\nBut enter critical section only as defined by the order of triggering of condition variables.\n";
	getchar();
	return 0;
}