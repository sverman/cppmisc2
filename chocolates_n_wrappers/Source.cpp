/*Little Bob loves chocolates, and goes to the store with $N money in his pocket. The price of each chocolate is $C. The store offers a discount: for every M wrappers he gives the store, he’ll get one chocolate for free. How many chocolates does Bob get to eat?*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int computeChocolates(int n, int c, int m);
int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int t,n,c,m;
    cin>>t;
    while(t--){
        cin>>n>>c>>m;
        int answer=0;
        // Computer answer
        answer = computeChocolates(n,c,m);
        cout<<answer<<endl;
    }
    return 0;
}

int computeChocolates(int n, int c, int m)
{
 int w,t1=0,t=0;
    w=n/c;
    while(w/m!=0)
    {
        
     t =t+w/m;        
     w =w/m+(w%m);
    }
    t1=t+n/c;
    return t1;
}
