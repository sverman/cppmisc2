#include<stdio.h>
#include<semaphore.h>
#include<pthread.h>
pthread_t tid1,tid2;// thread ids created here. needs to be global
sem_t sem1,sem2;// declare semaphores for the two threads
int sem_val1,sem_val2; // variable to hold the current value ofa semaphore
void* threadf1 (void);
void* threadf2 (void);
void main()
{
printf("\n This program will print a sequence using semaphores\n");
printf("Now spawning threads...");

char* ptr1 = 's';
char* ptr2 = 'a';
// setup semaphores for the two threads in main
sem_init(&sem1,0,1);
sem_init(&sem2,0,0);
if((pthread_create(&tid1,NULL,threadf1,NULL)==0))
printf("thread1 spawned...\n");

if((pthread_create(&tid2,NULL,threadf2,NULL)==0))
printf("thread2 spawned...\n");

if(pthread_join(tid1,NULL))
printf("\nError joining the thread1 with main");

if(pthread_join(tid2,NULL))
printf("\nError joining the thread2 with main");

}
void* threadf1()
{
while(1)
{
sem_wait(&sem2);
printf("\nA");
sem_post(&sem1);
}
}

void* threadf2()
{
while(1)
{
sem_wait(&sem1);
printf("\nB");
sem_post(&sem2);
}
}
