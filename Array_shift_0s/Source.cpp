void ShiftZeroesToRight(ref int[] array)
        {
            int nNumPositives = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != 0)
                {
                    array[nNumPositives] = array[i];
                    nNumPositives++;
                }
            }

            for (int i = nNumPositives; i < array.Length; i++)
            {
                array[i] = 0;
            }
        }