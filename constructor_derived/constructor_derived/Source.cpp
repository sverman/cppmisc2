#include <iostream>
using namespace std;
class Base
{
public:
	int m_nValue;

	Base(int nValue=0)
		: m_nValue(nValue)
	{
		cout<<"parent constructor"<<endl;
	}
};
/////////////////////////////////////////
class Derived2: public Base
{
public:
	double m_dValue;
	// Call Base(int) constructor with value nValue! First demonstrated use of initializer list
	Derived2(double dValue=0.0, int nValue=0): Base(nValue), m_dValue(dValue)
	{
		cout<<"derived constructor";
	}
};


class Derived: public Base
{
	public:
	double m_dValue;
	
	Derived(double dValue=0.0): m_dValue(dValue)
	{
		cout<<"derived constructor"<<endl;
	}
};

int main()
{
	//Base cBase(5); // use Base(int) constructor
	Derived cDerived(4);
	Derived2 dDerived(4,9);
	cout<<"\ncDerived.m_dValue "<<cDerived.m_dValue<<"\ncDerived.m_nValue "<<cDerived.m_nValue<<endl;
	cout<<"dDerived.m_dValue "<<dDerived.m_dValue<<"\ndDerived.m_nValue "<<dDerived.m_nValue<<endl;
	return 0;
}