#include <iostream>
#include <stdlib.h>
using namespace std;
int binarySearch(int arr[], int low, int high, int num);
int main()
{
	int num;
	int arr[10]= {2,4,6,8,10,12,14,16,18,20};
	cout<<"Welcome to BINARY SEARCH...";
	cout<<"\n Enter a number to search in {2,4,6,8,10,12,14,16,18,20} >>";
	cin>>num;
	int index = binarySearch(arr,0,9,num);
	cout<<"\nindex="<<index;
	return 0;
}
int binarySearch(int arr[], int low, int high, int num)
{
	int mid = low +(high-low)/2;
	if(low<=high)
	{
	if(arr[mid]==num)
		return mid;
	else if (arr[mid]<num)
		binarySearch(arr,mid+1,high,num);
	else
		binarySearch(arr,low,mid-1,num);
	}
	else return -1;
}